import 'package:get_it/get_it.dart';
import 'package:weather_app/apis/weather.dart';
import 'package:weather_app/models/weather_controller.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  locator.registerLazySingleton<WeatherController>(() => WeatherController());
  locator.registerLazySingleton<WeatherApi>(() => WeatherApi());
}
