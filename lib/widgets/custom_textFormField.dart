import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    Key? key,
    this.autofocus = false,
    this.onTap,
    this.valueChanged,
    this.onEditingCompleted,
    this.readOnly = false,
    this.obsecure = false,
    this.keyboardType,
    this.suffixIcon = null,
    @required this.hintText,
    @required this.controller,
  }) : super(key: key);

  final ValueChanged? valueChanged;
  final bool autofocus;
  final String? hintText;
  final TextEditingController? controller;
  final VoidCallback? onTap;
  final VoidCallback? onEditingCompleted;
  final TextInputType? keyboardType;
  final bool obsecure;
  final bool readOnly;
  final Widget? suffixIcon;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      readOnly: readOnly,
      obscureText: obsecure,
      autocorrect: false,
      autofocus: autofocus,
      controller: controller,
      validator: (value) {
        if (value!.isEmpty)
          return 'Cannot be empty';
        else
          return null;
      },
      onChanged: valueChanged,
      onTap: onTap,
      keyboardType: keyboardType,
      onEditingComplete: onEditingCompleted,
      decoration: InputDecoration(
        suffixIcon: suffixIcon,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        fillColor: Colors.white,
        filled: true,
        hintText: hintText,
        hintStyle: TextStyle(color: Color(0xff949594)),
        contentPadding: EdgeInsets.only(left: 20, right: 20),
      ),
      style: TextStyle(fontSize: 13, color: Color(0xff222222)),
    );
  }
}
