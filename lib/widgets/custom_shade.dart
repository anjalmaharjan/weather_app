import 'package:flutter/material.dart';
import 'package:weather_app/utils/size_helper.dart';

class CustomShadeWidget extends StatelessWidget {
  const CustomShadeWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Container(
        height: displayHeight(context) / 3,
        width: displayWidth(context),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.56),
              blurRadius: 20,
              spreadRadius: 60,
            )
          ],
        ),
      ),
    );
  }
}
