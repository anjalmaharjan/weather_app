import 'package:flutter/material.dart';
import 'package:weather_app/utils/color_helper.dart';
import 'package:weather_app/utils/size_helper.dart';

class CustomBtn extends StatelessWidget {
  final String? text;
  final double? width;
  final VoidCallback? onPressed;
  const CustomBtn(
      {Key? key, required this.text, required this.onPressed, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Colors.blue,
      height: displayHeight(context) / 20,
      minWidth: width,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadiusDirectional.circular(22)),
      onPressed: onPressed,
      child: Text(
        text!.toUpperCase(),
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 16,
          color: textColor,
        ),
      ),
    );
  }
}
