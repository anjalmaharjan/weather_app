import 'package:flutter/material.dart';

class TemperatureTextTile extends StatelessWidget {
  final String? headingText, temperature, condition;
  final String? icon;
  const TemperatureTextTile({
    Key? key,
    this.headingText,
    this.condition,
    this.temperature,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          headingText.toString(),
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
        const SizedBox(height: 10),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              temperature.toString().toUpperCase(),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 80),
            ),
            Row(
              children: [
                if (icon!.isNotEmpty)
                  Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      image:
                          DecorationImage(image: NetworkImage(icon.toString())),
                    ),
                  ),
                Text(
                  condition.toString(),
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
