import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../enum.dart';

class BaseController extends GetxController {
  var _state = ViewState.Idle.obs;
  ViewState get state => _state.value;

  void setState(ViewState newState) {
    _state.value = newState;
  }
}
