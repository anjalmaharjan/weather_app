import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:weather_app/utils/config.dart';

class WeatherApi {
  final Dio _dio = Dio();
  final String _baseUrl = BASE_URL;
  WeatherApi() {
    _dio.options.baseUrl = _baseUrl;
    _dio.options.connectTimeout = 5000;
  }
  Future<Weather?> fetchWeatherData({String? location}) async {
    try {
      final response = await _dio.get(
        '/v1/current.json?key=f4db82fea8ab4511a1c165126222004&q=$location',
      );
      if (response.statusCode == 200) return Weather.fromJson(response.data);
    } on DioError catch (e) {
      switch (e.response?.statusCode) {
        case 400:
          Get.snackbar("Error", "${e.response?.data["error"]}",
              backgroundColor: Colors.white);
          return null;

        case 404:
          Get.snackbar("Error", "${e.response?.data["error"]}",
              backgroundColor: Colors.white);
          return null;

        case 502:
          Get.snackbar("Error", "Something went wrong.");
          return null;

        default:
          return null;
      }
    }
    return null;
  }
}
