import 'package:weather_app/apis/weather.dart';
import 'package:weather_app/injector/injector.dart';
import 'package:weather_app/models/weather_model.dart';

class WeatherController {

  Future<Weather?> getWeather({String? location}) async {
    return locator
        .get<WeatherApi>()
        .fetchWeatherData(location:location);
  }
}
