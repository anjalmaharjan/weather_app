import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:weather_app/injector/injector.dart';

import 'app/routes/app_pages.dart';

void main() {
  setupLocator();
  runApp(
    
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Application",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    ),
  );
}
