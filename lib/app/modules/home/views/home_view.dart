import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/app/modules/help_screen/views/help_screen_view.dart';
import 'package:weather_app/enum.dart';
import 'package:weather_app/utils/size_helper.dart';
import 'package:weather_app/widgets/custom_btn.dart';
import 'package:weather_app/widgets/custom_textFormField.dart';
import 'package:weather_app/widgets/temperature_text_tile.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final HomeController _homeView = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        elevation: 0,
      ),
      body: Form(
        key: _homeView.key,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28.0),
          child: ListView(
            shrinkWrap: true,
            physics: BouncingScrollPhysics(),
            children: [
              Align(
                  alignment: Alignment.topRight,
                  child: MaterialButton(
                    padding: EdgeInsets.zero,
                    minWidth: 0,
                    onPressed: () => Get.to(() => HelpScreenView()),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.info),
                        const SizedBox(width: 6),
                        Text(
                          'For Help',
                        ),
                      ],
                    ),
                  )),
              const SizedBox(height: 10),
              const Text(
                'Weather App',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
              ),
              const SizedBox(height: 10),
              const Text(
                'This application is used to forecast the weather of your region.',
                style: TextStyle(fontSize: 15),
              ),
              SizedBox(height: displayHeight(context) / 20),
              Obx(
                () => TemperatureTextTile(
                  headingText: 'Temperature in C',
                  condition: _homeView.condition.value,
                  temperature: _homeView.tempC.value,
                  icon: _homeView.icon.value,
                ),
              ),
              SizedBox(height: displayHeight(context) / 25),
              Obx(
                () => TemperatureTextTile(
                  headingText: 'Temperature in F',
                  condition: _homeView.condition.value,
                  temperature: _homeView.tempF.value,
                  icon: _homeView.icon.value,
                ),
              ),
              SizedBox(height: displayHeight(context) / 12),
              const Text(
                'Location',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              const SizedBox(height: 10),
              CustomTextFormField(
                hintText: "Enter your location",
                controller: _homeView.nameController,
                valueChanged: (value) {
                  _homeView.isPressed.value = true;
                  _homeView.btnTextChange();
                },
              ),
              const SizedBox(height: 20),
              Obx(
                () => _homeView.state == ViewState.Busy
                    ? Center(
                        child: CircularProgressIndicator.adaptive(),
                      )
                    : CustomBtn(
                        text: _homeView.btnTextValue.value.toString(),
                        onPressed: () => _homeView.getWeatherData(),
                        width: displayWidth(context),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
