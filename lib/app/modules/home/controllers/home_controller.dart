import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/base_model/base_model.dart';
import 'package:weather_app/enum.dart';
import 'package:weather_app/injector/injector.dart';
import 'package:weather_app/models/weather_controller.dart';
import 'package:weather_app/models/weather_model.dart';

class HomeController extends BaseController {
  final TextEditingController nameController = TextEditingController();
  final key = GlobalKey<FormState>();

  RxBool isPressed = false.obs;
  RxString btnTextValue = 'save'.obs;
  Weather? weather;
  List weatherList = [];
  RxString tempC = '0'.obs;
  RxString tempF = '0'.obs;
  RxString condition = ''.obs;
  RxString icon = ''.obs;

  void btnTextChange() {
    if (isPressed.value && nameController.text.isNotEmpty) {
      btnTextValue.value = 'update';
    } else if (nameController.text.isEmpty) {
      btnTextValue.value = 'save';
    }
  }

  void tempChange() {
    if (weather != null) {
      tempC.value = weather!.current!.tempInC.toString();
      tempF.value = weather!.current!.tempInF.toString();
      condition.value = weather!.current!.condition!.text.toString();
      icon.value = "https://" +
          weather!.current!.condition!.icon.toString().substring(2);
    }
  }

  void getWeatherData() async {
    if (key.currentState!.validate()) {
      setState(ViewState.Busy);
      weather = await locator
          .get<WeatherController>()
          .getWeather(location: nameController.text.trim());
      tempChange();
      setState(ViewState.Retrieved);
    }
  }

  void getWeatherDataFromLatLong() async {
    weather = await locator
        .get<WeatherController>()
        .getWeather(location: "27.6716759,85.3298665");
    tempChange();
  }

  @override
  void onInit() {
    super.onInit();
    btnTextChange();
    getWeatherDataFromLatLong();
  }

  @override
  void onReady() {
    super.onReady();
    getWeatherDataFromLatLong();
  }

  @override
  void onClose() {}
}
