import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:weather_app/app/modules/home/views/home_view.dart';
import 'package:weather_app/utils/color_helper.dart';
import 'package:weather_app/utils/size_helper.dart';
import 'package:weather_app/widgets/custom_btn.dart';
import 'package:weather_app/widgets/custom_shade.dart';

import '../controllers/help_screen_controller.dart';

class HelpScreenView extends GetView<HelpScreenController> {
  final HelpScreenController _helpScreenController =
      Get.put(HelpScreenController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        elevation: 0,
        systemOverlayStyle: const SystemUiOverlayStyle(statusBarColor: bgColor),
      ),
      body: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Stack(
            children: [
              Container(
                height: displayHeight(context),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/help_screen.jpg'),
                        fit: BoxFit.cover)),
              ),
              const CustomShadeWidget(),
              Positioned(
                bottom: 80,
                left: 30,
                child: Column(
                  children: [
                    const Text(
                      'We\nshow\nweather\nfor you.',
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: textColor),
                    ),
                    const SizedBox(height: 20),
                    CustomBtn(
                      text: 'skip',
                      onPressed: () => Get.offAll(() => HomeView()),
                      width: displayWidth(context) / 3,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
