import 'package:get/get.dart';
import 'package:weather_app/app/modules/home/views/home_view.dart';

class HelpScreenController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    timer();
  }

  void timer() async {
    await Future.delayed(
        Duration(seconds: 5), () => Get.offAll(() => HomeView()));
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
