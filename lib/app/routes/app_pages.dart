import 'package:get/get.dart';

import '../modules/help_screen/bindings/help_screen_binding.dart';
import '../modules/help_screen/views/help_screen_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HELP_SCREEN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.HELP_SCREEN,
      page: () => HelpScreenView(),
      binding: HelpScreenBinding(),
    ),
  ];
}
